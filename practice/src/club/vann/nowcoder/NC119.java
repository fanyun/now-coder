package club.vann.nowcoder;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * <p>难度：Medium</p>
 * <p>题目：最小的K个数</p>
 * <p>描述：给定一个数组，找出其中最小的K个数。例如数组元素是4,5,1,6,2,7,3,8这8个数字，则最小的4个数字是1,2,3,4。如果K>数组的长度，那么返回一个空的数组
 * 示例1
 * 输入
 * 复制
 * [4,5,1,6,2,7,3,8],4
 * 返回值
 * 复制
 * [1,2,3,4]</p>
 * @author vann
 * @program now-coder
 * @description
 * @date 2021-03-13:17:54:13
 */
public class NC119 {
    public static void main(String[] args) {
        NC119 nc119 = new NC119();
    }

    public ArrayList<Integer> GetLeastNumbers_Solution(int [] input, int k) {
        ArrayList<Integer> ans = new ArrayList<>();
        int len = input.length;
        if(k > len) {
            return ans;
        }

        Arrays.sort(input);
        for(int i = 0; i < k; i ++) {
            ans.add(input[i]);
        }
        return ans;
    }

    static class TestCase {
        public static int[] INPUT = {4,5,1,6,2,7,3,8};
        public static int K = 4;
    }
}
