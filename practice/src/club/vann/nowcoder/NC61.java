package club.vann.nowcoder;

import java.util.Arrays;

/**
 * <p>难度：Easy</p>
 * <p>题目：两数之和</p>
 * <p>描述：给出一个整数数组，请在数组中找出两个加起来等于目标值的数，
 * 你给出的函数twoSum 需要返回这两个数字的下标（index1，index2），需要满足 index1 小于index2.。注意：下标是从1开始的
 * 假设给出的数组中只存在唯一解
 * 例如：
 * 给出的数组为 {20, 70, 110, 150},目标值为90
 * 输出 index1=1, index2=2
 *
 *
 * 示例1
 * 输入
 * 复制
 * [3,2,4],6
 * 返回值
 * 复制
 * [2,3]</p>
 * @author vann
 * @program now-coder
 * @description
 * @date 2021-03-22:08:55:32
 */
public class NC61 {
    public static void main(String[] args) {
        NC61 nc61 = new NC61();

        System.out.println("Result["+Arrays.toString(TestCase.ANS)+"] : " + Arrays.toString(nc61.twoSum(TestCase.NUMBERS, TestCase.TARGET)));
        System.out.println("Result["+Arrays.toString(TestCase.ANS1)+"] : " + Arrays.toString(nc61.twoSum(TestCase.NUMBERS1, TestCase.TARGET1)));
    }

    /**
     *
     * @param numbers int整型一维数组
     * @param target int整型
     * @return int整型一维数组
     */
    public int[] twoSum (int[] numbers, int target) {
        // write code here
        int[] ans = new int[2];
        int len = numbers.length;
        for(int left = 0; left < len-1; left ++) {
            for(int right = left+1; right < len; right ++) {
                if(numbers[left] + numbers[right] == target) {
                    ans[0] = left + 1;
                    ans[1] = right + 1;
                    return ans;
                }
            }
        }
        return ans;
    }

    static class TestCase {
        public static int[] ANS = {2,3};
        public static int[] NUMBERS = {3,2,4};
        public static int TARGET = 6;

        public static int[] ANS1 = {1,2};
        public static int[] NUMBERS1 = {20, 70, 110, 150};
        public static int TARGET1 = 90;
    }
}
