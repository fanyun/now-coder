package club.vann.nowcoder;

import club.vann.nowcoder.common.TreeNode;

/**
 * <p>难度：Easy</p>
 * <p>题目：二叉树的镜像</p>
 * <p>描述：操作给定的二叉树，将其变换为源二叉树的镜像。
 * 比如：    源二叉树
 *             8
 *            /  \
 *           6   10
 *          / \  / \
 *         5  7 9 11
 *         镜像二叉树
 *             8
 *            /  \
 *           10   6
 *          / \  / \
 *         11 9 7  5
 * 示例1
 * 输入
 * 复制
 * {8,6,10,5,7,9,11}
 * 返回值
 * 复制
 * {8,10,6,11,9,7,5}</p>
 * @author vann
 * @program now-coder
 * @description
 * @date 2021-03-19:17:07:38
 */
public class NC72 {
    public static void main(String[] args) {
        NC72 nc72 = new NC72();

        System.out.println("Result["+TestCase.ANS+"] : " + nc72.Mirror(TestCase.ROOT));
    }

    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param pRoot TreeNode类
     * @return TreeNode类
     */
    public TreeNode Mirror (TreeNode pRoot) {
        // write code here
        if(pRoot == null) {
            return null;
        }

        TreeNode left = pRoot.left;
        TreeNode right = pRoot.right;
        pRoot.left = Mirror(right);
        pRoot.right = Mirror(left);
        return pRoot;
    }

    static class TestCase {
        public static TreeNode ANS = TreeNode.deserialize("[8,10,6,11,9,7,5]");
        public static TreeNode ROOT = TreeNode.deserialize("[8,6,10,5,7,9,11]");
    }
}
