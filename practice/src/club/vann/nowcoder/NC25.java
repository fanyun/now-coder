package club.vann.nowcoder;

import club.vann.nowcoder.common.ListNode;

import java.util.List;

/**
 * <p>难度：Easy</p>
 * <p>题目：删除有序列表中重复的元素</p>
 * <p>描述：删除给出链表中的重复元素（链表中元素从小到大有序），使链表中的所有元素都只出现一次
 * 例如：
 * 给出的链表为1\to1\to21→1→2,返回1 \to 21→2.
 * 给出的链表为1\to1\to 2 \to 3 \to 31→1→2→3→3,返回1\to 2 \to 31→2→3.
 *
 * 示例1
 * 输入
 * 复制
 * {1,1,2}
 * 返回值
 * 复制
 * {1,2}</p>
 * @author vann
 * @program now-coder
 * @description
 * @date 2021-03-23:17:48:58
 */
public class NC25 {
    public static void main(String[] args) {
        NC25 nc25 = new NC25();

//        System.out.println(nc25.deleteDuplicates(TestCase.HEAD));
        System.out.println(nc25.deleteDuplicates(TestCase.HEAD1));
    }

    /**
     *
     * @param head ListNode类
     * @return ListNode类
     */
    public ListNode deleteDuplicates (ListNode head) {
        // write code here
        if(head == null || head.next == null) {
            return head;
        }

        ListNode cur = head;
        ListNode next = null;

        while(cur != null) {
            next = cur.next;
            while(next != null && cur.val == next.val) {
                next = next.next;
            }
            cur.next = next;
            cur = cur.next;
        }
        return head;
    }

    static class TestCase {
        public static ListNode HEAD = ListNode.deserialize("[1,1,2]");
        public static ListNode HEAD1 = ListNode.deserialize("[1,1,2,2,3,3]");
    }
}
