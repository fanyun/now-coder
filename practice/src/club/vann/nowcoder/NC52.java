package club.vann.nowcoder;

import java.util.Stack;

/**
 * <p>难度：Easy</p>
 * <p>题目：有效括号</p>
 * <p>描述：给出一个仅包含字符'(',')','{','}','['和']',的字符串，判断给出的字符串是否是合法的括号序列
 * 括号必须以正确的顺序关闭，"()"和"()[]{}"都是合法的括号序列，但"(]"和"([)]"不合法。
 * 示例1
 * 输入
 * 复制
 * "["
 * 返回值
 * 复制
 * false
 * 示例2
 * 输入
 * 复制
 * "[]"
 * 返回值
 * 复制
 * true</p>
 * @author vann
 * @program now-coder
 * @description
 * @date 2021-03-18:18:25:43
 */
public class NC52 {
    public static void main(String[] args) {
        NC52 nc52 = new NC52();

        System.out.println("Result["+TestCase.ANS+"] : " + nc52.isValid1(TestCase.S));
        System.out.println("Result["+TestCase.ANS1+"] : " + nc52.isValid1(TestCase.S1));
        System.out.println("Result["+TestCase.ANS2+"] : " + nc52.isValid1(TestCase.S2));
        System.out.println("Result["+TestCase.ANS3+"] : " + nc52.isValid1(TestCase.S3));
        System.out.println("Result["+TestCase.ANS4+"] : " + nc52.isValid1(TestCase.S4));
        System.out.println("Result["+TestCase.ANS5+"] : " + nc52.isValid1(TestCase.S5));
        System.out.println("Result["+TestCase.ANS6+"] : " + nc52.isValid1(TestCase.S6));
    }

    /**
     *
     * @param s string字符串
     * @return bool布尔型
     */
    public boolean isValid (String s) {
        // write code here
        if(s == null || s.length() == 0) {
            return false;
        }

        char[] ch = s.toCharArray();
        int len = ch.length;
        if(len % 2 !=  0) {
            return false;
        }

        Stack<Character> stack = new Stack<>();
        for(int i = 0; i < len; i ++) {
            char c = ch[i];
            if(c == '(' || c == '{' || c == '[') {
                stack.push(c);
            } else {
                if(!stack.isEmpty() && (stack.peek() == '(' && c ==')' || (stack.peek() == '{' && c =='}') || (stack.peek() == '[' && c ==']'))) {
                    stack.pop();
                } else {
                    return false;
                }
            }
        }

        return stack.isEmpty();
    }

    /**
     *
     * @param s string字符串
     * @return bool布尔型
     */
    public boolean isValid1 (String s) {
        // write code here
        if(s == null || s.length() == 0) {
            return false;
        }

        boolean flag = true;
        while(flag){
            int len = s.length();
            s=s.replace("()","");
            s=s.replace("[]","");
            s=s.replace("{}","");
            if(len == s.length()){
                flag=false;
            }
        }

        return s.length() == 0;
    }

    static class TestCase {
        public static boolean ANS = true;
        public static String S = "{}";

        public static boolean ANS1 = true;
        public static String S1 = "{}()";

        public static boolean ANS2 = true;
        public static String S2 = "({})";

        public static boolean ANS3 = false;
        public static String S3 = "[";

        public static boolean ANS4 = false;
        public static String S4 = "[]({)}";

        public static boolean ANS5 = false;
        public static String S5 = "(()])}[}[}[]][}}[}{})][[(]({])])}}(])){)((){";

        public static boolean ANS6 = false;
        public static String S6 = "}{[{]}[{[(([}]({(]([(){){{)][{[";
    }
}
