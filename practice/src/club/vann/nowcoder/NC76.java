package club.vann.nowcoder;

import java.util.Stack;

/**
 * <p>难度：Medium</p>
 * <p>题目：用两个栈实现队列</p>
 * <p>描述：用两个栈来实现一个队列，完成队列的Push和Pop操作。 队列中的元素为int类型。</p>
 * @author vann
 * @program now-coder
 * @description
 * @date 2021-04-20:15:28:12
 */
public class NC76 {
    public static void main(String[] args) {
        NC76 nc76 = new NC76();


    }

    class CustomQueue {
        Stack<Integer> stack1 = new Stack<Integer>();
        Stack<Integer> stack2 = new Stack<Integer>();

        public void push(int node) {
            stack1.push(node);
        }

        public int pop() {
            if(!stack2.isEmpty()) {
                return stack2.pop();
            } else {
                while(!stack1.isEmpty()) {
                    stack2.push(stack1.pop());
                }
                return stack2.pop();
            }
        }

    }
}
