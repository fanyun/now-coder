package club.vann.nowcoder;

import java.util.Locale;

/**
 * <p>难度：Medium</p>
 * <p>题目：大数加法</p>
 * <p>描述：以字符串的形式读入两个数字，编写一个函数计算它们的和，以字符串形式返回。
 * （字符串长度不大于100000，保证字符串仅由'0'~'9'这10种字符组成）
 * 示例1
 * 输入
 * 复制
 * "1","99"
 * 返回值
 * 复制
 * "100"
 * 说明
 * 1+99=100 </p>
 * @author vann
 * @program now-coder
 * @description
 * @date 2021-03-10:15:09:05
 */
public class NC1 {
    public static void main(String[] args) {
        NC1 nc1 = new NC1();

        System.out.println("Result["+TestCase.ANS+"] : " + nc1.solve(TestCase.S, TestCase.T));
        System.out.println("Result["+TestCase.ANS1+"] : " + nc1.solve(TestCase.S1, TestCase.T1));
        System.out.println("Result["+TestCase.ANS2+"] : " + nc1.solve(TestCase.S2, TestCase.T2));
    }

    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     * 计算两个数之和
     * @param s string字符串 表示第一个整数
     * @param t string字符串 表示第二个整数
     * @return string字符串
     */
    public String solve (String s, String t) {
        // write code here
        char[] ch1 = s.toCharArray();
        char[] ch2 = t.toCharArray();
        int len1 = ch1.length;
        int len2 = ch2.length;

        int len = Math.max(len1, len2) + 1;
        char[] ans = new char[len];

        int pow = 0, cur = 0;
        int index = len-1;
        int index1 = 0, index2 = 0;
        while(index1 < len1 || index2 < len2) {
            int sum = pow;
            if(index1 < len1) {
                sum += ch1[len1-1-index1]-'0';
            }

            if(index2 < len2) {
                sum += ch2[len2-1-index2]-'0';
            }

            cur = sum % 10;
            pow = sum / 10;
            ans[index--] = (char) (cur + '0');
            index1 ++;
            index2 ++;
        }

        ans[index] = (char) (pow+'0');
        index = 0;
        while(index < len && ans[index] == '0') {
            index ++;
        }
        if(index >= len) {
            return "0";
        } else {
            return new String(ans, index, len-1-index+1);
        }
    }

    static class TestCase {
        public static String ANS = "100";
        public static String S = "1";
        public static String T = "99";

        public static String ANS1 = "100";
        public static String S1 = "01";
        public static String T1 = "99";

        public static String ANS2 = "0";
        public static String S2 = "000";
        public static String T2 = "0";
    }
}
