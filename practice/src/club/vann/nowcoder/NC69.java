package club.vann.nowcoder;

import club.vann.nowcoder.common.ListNode;

import java.util.List;

/**
 * <p>难度：Medium</p>
 * <p>题目：链表中倒数第K个节点</p>
 * <p>描述：输入一个链表，输出该链表中倒数第k个结点。
 * 示例1
 * 输入
 * 复制
 * {1,2,3,4,5},1
 * 返回值
 * 复制
 * {5}</p>
 * @author vann
 * @program now-coder
 * @description
 * @date 2021-03-23:09:28:24
 */
public class NC69 {
    public static void main(String[] args) {
        NC69 nc69 = new NC69();

        System.out.println(nc69.FindKthToTail(TestCase.NODE, TestCase.K));
    }

    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param pHead ListNode类
     * @param k int整型
     * @return ListNode类
     */
    public ListNode FindKthToTail (ListNode pHead, int k) {
        // write code here
        if(pHead == null || pHead.next == null) {
            return pHead;
        }

        // 虚拟头节点
        ListNode node0 = new ListNode(0);
        node0.next = pHead;

        int count = 0;
        ListNode node1 = pHead;
        ListNode node2 = node0;
        while(node1 != null) {
            node1 = node1.next;
            count ++;
            if(count >= k) {
                node2 = node2.next;
            }
        }
        return node2;
    }

    static class TestCase {
        public static ListNode NODE = ListNode.deserialize("[1,2,3,4,5,6]");
        public static int K = 2;
    }
}
