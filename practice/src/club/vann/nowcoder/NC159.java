package club.vann.nowcoder;

/**
 * <p>难度：Medium</p>
 * <p>题目：最小生成树</p>
 * <p>描述：一个有n户人家的村庄，有m条路连接着。村里现在要修路，每条路都有一个代价，现在请你帮忙计算下，最少需要花费多少的代价，就能让这n户人家连接起来。
 * 示例1
 * 输入
 * 复制
 * 3,3,[[1,3,3],[1,2,1],[2,3,1]]
 * 返回值
 * 复制
 * 2</p>
 * @author vann
 * @program now-coder
 * @description
 * @date 2021-03-24:21:01:15
 */
public class NC159 {
    public static void main(String[] args) {

    }

    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 返回最小的花费代价使得这n户人家连接起来
     * @param n int n户人家的村庄
     * @param m int m条路
     * @param cost int二维数组 一维3个参数，表示连接1个村庄到另外1个村庄的花费的代价
     * @return int
     */
    public int miniSpanningTree (int n, int m, int[][] cost) {
        // write code here
        return 0;
    }

    static class TestCase {
        public static int ANS = 2;
        public static int N = 3;
        public static int M = 3;
        public static int[][] COST = {{1, 3, 3}, {1, 2, 1}, {2, 3, 1}};
    }
}
